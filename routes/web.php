<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//make auth model available on route method
Auth::routes();

//route to the home view using get
Route::get('/home', 'HomeController@index')->name('home');

//route to the create questionnaire view. 
Route::get('/questionnaires/create', 'QuestionnaireController@create');

//route for the questionnaire store, show and destroy using post, get and delete.
Route::post('/questionnaires', 'QuestionnaireController@store');
Route::get('/questionnaires/{questionnaire}', 'QuestionnaireController@show');
Route::delete('/questionnaires/{questionnaire}', 'QuestionnaireController@destroy');

//route  to the create question view using get
Route::get('/questionnaires/{questionnaire}/questions/create', 'QuestionController@create');

//route  to the edit questionnaire view using get
Route::get('/questionnaires/{questionnaire}/edit', 'QuestionnaireController@edit');

//route  to the edit questions and answers view using get
Route::get('/questions/{question}/edit', 'QuestionController@edit');

//route to the store and destroy questionnaire question using delete and post
Route::post('/questionnaires/{questionnaire}/questions', 'QuestionController@store');
Route::delete('/questionnaires/{questionnaire}/questions/{question}', 'QuestionController@destroy');

//route  show and store survey questionnaire using get and post
Route::get('/surveys/{questionnaire}-{slug}', 'SurveyController@show');
Route::post('/surveys/{questionnaire}-{slug}', 'SurveyController@store');

//routes for the update questionnaire and question route uisng put 
Route::put('/questionnaires/{questionnaire}', 'QuestionnaireController@update'); 
Route::put('/questions/{question}', 'QuestionController@update'); 

//route to the export view for data in excel format using get method
Route::get('export', 'SurveyController@exportData'); 