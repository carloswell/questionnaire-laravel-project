<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/*
* class responsible to make the connection between the questionnaire db table with the system
* with protected set to fillable of the table fields and functions patch, publicPatch, user, questions and surveys  
* to work the main part of the system development the questionnaire.
*/ 

class Questionnaire extends Model
{
    protected $fillable = [
        'title',
        'aim',
    ];

    public function path(){  //function responsible to return a url patch for the questiionnaire controller.

        return url('/questionnaires/' . $this->id); //return the url path with the id choosed.
    }

    public function publicPath(){  //function responsible to return the url public path for the survey used to take the survey 

        return url('/surveys/' .$this->id. '-'. Str::slug($this->title)); //return the path to be send to the usr responser
    }

    public function user(){   //function responsible to make  the relationship between the admin and the questionnaire

        return $this->belongsTo(User::class);  //return the relation many questionniare may belong to one user admin
    }

    public function questions(){   //function responsible to to make the relationship between the questionnaire and the questions

        return $this->hasMany(Question::class);   //return a questionnaire may have many questions.
    }

    public function surveys(){   //function responsible to make the relationship between the questionnaire and the survey take it

        return $this->hasMany(Survey::class); //return a questionnaire may have many surveys to be responded. 
    }
}
