<?php

namespace App\Http\Controllers;

use App\Questionnaire;  // calling the questionnaire model 
use App\Question;  
use App\User;
use Illuminate\Http\Request;

/*
*The class questionnaire one of the mean classes of the system responsible to questionnaire
*creation, store it on db edit, update and destroy functions to take care of all information
*related to the questionnaire scoope, also use a construct function to validate the auth.  
* 
*/

class QuestionnaireController extends Controller
{
    public function __construct(){ //function construct to validate the auth with middleware() method.

        $this->middleware('auth'); // callng the auth
    }

    public function create(){ // function responsible to create and return the view of questionnaire/create page.
        return view('questionnaire.create');
    }

    public function store(){   //function responsible to store the data added in the questionnaire creation to the db. 
        $data = request()->validate([ //validate the questionnaire table fields
            'title' => 'required',
            'aim' => 'required',
        ]);


       $questionnaire = auth()->user()->questionnaires()->create($data);  // questionnaire vairable to store the  auth user and the questionnaire created that store in the vairable $data

        return redirect('/questionnaires/'.$questionnaire->id); //return the questionnaire page created. 
    }

    public function show(\App\Questionnaire $questionnaire){ //function show to load the main questionnaire page with the questions and answers created and if any response 

        $questionnaire->load('questions.answers.responses');  // loading method to request the questions and answers and responses from the questionnaire.


        return view('questionnaire.show', compact('questionnaire')); //return use view of show.blade.php fromquestionnaire folder. 
    }


    public function edit( $id) //function respnsible to edit questionnaire title and aim fields
    {

        $questionnaire = Questionnaire::find($id); //find the id model of questinnaire in the db 

        return view('questionnaire.edit', compact('questionnaire')); //return edit.blade.php view in the questionnaire folder
    }


    public function update( Request $request, $id)  //function responsible to update the changes made in the questionnaire edit view.
    {

        $this->validate($request, array( // validate the table fields.
            'title' => 'required',
            'aim' => 'required',
        ));

        $questionnaire = Questionnaire::find($id);  // find the questionnaire id in the model of the questionnaire db 
        $questionnaire->title = $request->input('title');
        $questionnaire->aim = $request->input('aim');
        $questionnaire->save();

        return redirect('/questionnaires/'.$questionnaire->id); //return the questionnaire already updated.
    }


    public function destroy(Questionnaire $questionnaire, Question $question ){  //function responsible to delete the questionnaire
      
        $question->answers()->delete();   //use the delete method come from the route to delete the answers in the question

        $question->delete();   //use the delete method come from the route to delete the question
        
        $questionnaire->delete();  //use the delete method come from the route 

        return redirect('home');  //return the main home page
    }
}
