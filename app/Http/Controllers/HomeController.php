<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*
*The class Homecontroller responsible to show the first view in the system the home view
*dashboard and where can access the index function responsible to the view of home.blade.php of the system  
*/

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
     public function index()  //function index to sow the mian home page of the system
    {
        $questionnaires = auth()->user()->questionnaires;  //store the value of auth usr of the questionnaire.
        
        return view('home', compact('questionnaires')); //return home.
    }
}
