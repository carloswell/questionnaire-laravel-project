<?php

namespace App\Http\Controllers;
use App\Questionnaire;           //find the questionnaire model provided
use App\Question;              //find the question model provided
use App\Answer;

use Illuminate\Http\Request;

/*
*The class question responsible to questions and answers 
*creation, store it on db edit, update and destroy functions to take care of all information
*related to the questions and answers scoope.  
* 
*/

class QuestionController extends Controller
{
    public function create(Questionnaire $questionnaire){    // function responsible to create and return the view of question/create page.

        return view('question.create', compact('questionnaire'));
    }

    public function store(Questionnaire $questionnaire){   //function responsible to store the data added in the questions and answers creation to the db.

       // dd(request()->all());

       $data = request()->validate([
        'question.question' => 'required',
        'answers.*.answer' => 'required',
        ]);

        $question = $questionnaire -> questions()->create($data['question']); //question variable to store the question data created in the questionnaire
        $question->answers()->createMany($data['answers']);

        return redirect('/questionnaires/'.$questionnaire->id); //return the the questionnaire page id.
    }

    public function edit($id)  //function respnsible to edit questions of the questionnaire provided.
    {
        $questionnaire = Questionnaire::find($id);
        $question = Question::find($id);  //find the id model of question in the db 
      
        return view('question.edit', compact('question'));  //return edit.blade.php view in the question folder
    }

    public function update(Request $request, $id)    //function responsible to update the changes made in the question edit view.
    {
        // validate the table fields.
        $data = request()->validate([
            'question.question' => 'required',
            'answers.*.answer' => 'required',
            ]);

            $question-> questions()->input($request['question']); //question variable to store the question data created in the questionnaire
            $question->answers()->inputMany($request['answers']);
            $question->save();

        

        return redirect('/questionnaires/{questionnaire}'. $questionnaire->id);   //return the question already updated.
    }
    

    public function destroy(Questionnaire $questionnaire, Question $question ){   //function responsible to delete the question

        $question->answers()->delete();   //use the delete method come from the route to delete the answers in the question

        $question->delete();   //use the delete method come from the route to delete the question

        return redirect($questionnaire->path());  //return the questionnaire page back.
    }

}
