<?php

namespace App\Http\Controllers;

use App\Questionnaire;   //calling the Questionnaire model.
use App\SurveyResponse;  //calling the SurveyRespnse model.
use Excel;  //calling the excel services installed
use Maatwebsite\Excel\Concerns\FromCollection;  //callin maatwebsite to hand excel files on collections 

/*
* class responsible to controll the questionnaire responded and their questions and answers responded.
* usign the help of function show , store to show the questinnaire survey responsed and store it in the db. 
*/

class SurveyController extends Controller
{
    public function show(Questionnaire $questionnaire, $slug){   //function responsible to load the questions and answers ans show it in the view page. 

        $questionnaire->load('questions.answers');  //loading the questions and answers with load method.

        return view('survey.show', compact('questionnaire')); //return the view to the user or suurvey responded.
    }

    public function store(Questionnaire $questionnaire){   //function responsible to store the questions and answers responded by the user to the db
       
       // dd(request()->all());
        $data = request()->validate([           //require validation  of the survey fields in the table suveyResponses
            'responses.*.answer_id' => 'required',
            'responses.*.question_id' => 'required',
            'survey.name' => 'required',
            'survey.email' => 'required|email',
            
        ]);

        $survey = $questionnaire->surveys()->create($data['survey']); //variable survey to store the value of questionnaire survey the data from the survey responded 
        $survey->responses()->createMany($data['responses']); //making the many relation of survey questinnaire and responses
       
        return '<h1>Thank you for your time in answer this survey :))!!</h1>';  //message return to the suvey responder 
        
    }

    function exportData() //function responsibe to download the survey responses in a excel file or xlsx extension.
    {
        return Excel::download(new DataExport, 'responses.xlsx'); //return  the download on /export view. 
    }
}
/*
*New class to hand the data from survey response and export it for donwload,
* using the function collection to grap the responses and the table 
* and send to the export functon.
 */
class DataExport implements FromCollection{ 

    function collection(){  //function to collect all the responses to send to export.

        return SurveyResponse::all(); //return the survey responses.
    }
}
