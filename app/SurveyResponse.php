<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* class responsible to make the connection between the survey responses db tables with the questionnaire system
* with protected set to guarded of the table fields and function survey to make the 
*table relationship between the objects. 
*/ 

class SurveyResponse extends Model
{
    protected $guarded = [];

    public function survey(){  //function responsible to make the relation between the survey respnses and the survey

        return $this->belongsTo(Survey::class); //return the relation many responses may belongs to a survey.

    }
    
}
