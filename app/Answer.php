<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* class responsible to make the connection between the answer db table with the questinniare system
* with protected set to guarded of the table fileds and functions question and responses to make the 
*table relationship between the objects. 
*/ 

class Answer extends Model
{
    protected $fillable =[
        'question_id',
        'answer',
    ];

    public function question(){ //function responsible to make the relationship between the answers and question.

        return $this->belongsTo(Question::class); // return many answers choices may  belongs to one question
    }

    public function responses(){   //functin responsible to make the relantionship between the reponses from the user and the answers choice

        return $this->hasMany(SurveyResponse::class); // return a answer may has many survey reponses. 
    }
}
