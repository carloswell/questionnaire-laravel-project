<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* class responsible to make the connection between the survey db table with the questionnaire system
* with protected set to guarded of the table fields and functions questionnaire and responses to make 
*table relationship between the objects. 
*/ 

class Survey extends Model
{
    protected $guarded = [];

    public function questionnaire(){    //function responsible to make the relationship between the survey and questionnaire.
        
        return $this->belongsTo(Questionnaire::class);  //return the relation many surveys belongs to a questionnaire
    }

    public function responses(){ //fuction responsible to make the relationship between the survey and its responses.

        return $this->hasMany(SurveyResponse::class);  //return a survey may has many responses.
    }

}
