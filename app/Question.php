<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
* class responsible to make the connection between the questin db table with the questionnaire system
* with protected set to guarded of the table fields and functions questionnaire, answers and responses to make the 
*table relationship between the objects. 
*/ 

class Question extends Model
{
    protected $fillable = [
        'questionnaire_id',
        'question',
    ];

    public function questionnaire(){    //function responsible to make the relationship between the question and questionnaire.

        return $this->belongsTo(Questionnaire::class);  // return many questions may  belongs to one questionnaire
    }

    public function answers(){   //function responsible to make the relantionship between the answers choices and the question

        return $this->hasMany(Answer::class);   //return a question may has many answers choice.
    }

    public function responses(){   //functin responsible to make the relantionship between the reponses from the user and the question

        return $this->hasMany(SurveyResponse::class);  // return a questionmay has many survey responses 
    }
}
