<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a questionnaire');

//add a test user
$I->haveRecord('users', [
    'id'=> '15',
    'name'=> 'wellington',
    'email'=> 'wellington@example.com',
    'password'=> 'omelhor1980'
    ]);
    
    //Add test questionnaire
$I->haveRecord('questionnaires', [
    'id'=> '10',
    'user_id'=> '01',
    'Title' => 'The impact of covid-19 situation changing the world',
    'aim' => 'gathering infromation for analysis of the world health situation',
    ]);
    

//add test question
$I->haveRecord('questions', [
'id'=> '23',
'questionnaire_id'=>'10',
'question' => 'Did covid situation disturb ur life?',
]);

//add test answer choice
//multiple choices question
$I->haveRecord('answers', [   
'id'=> '25',
'question_id'=>'23',
'answer' => 'very hard',

]);

//When
$I->amOnPage('/home/questionnaires/10');
//and
$I->see('content');

//then
$I->seeElement('a', [ 'name'=>'23']);
//and
$I->click('Delete');

//then
$I->amOnPage('/home/questionnaires/10');
//and
$I->dontSeeElement('a',['name'=> '23']);