<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a question');

//add a test user
$I->haveRecord('users', [
    'id'=> '15',
    'name'=> 'wellington',
    'email'=> 'wellington@example.com',
    'password'=> 'omelhor1980'
    ]);
    
    //Add test questionnaire
$I->haveRecord('questionnaires', [
    'id'=> '10',
    'user_id'=> '01',
    'Title' => 'The impact of covid-19 situation changing the world',
    'aim' => 'gathering infromation for analysis of the world health situation',
    ]);
    

//add test question
$I->haveRecord('questions', [
'id'=> '23',
'questionnaire_id'=>'10',
'question' => 'Did covid situation disturb ur life?',
]);

//add test answer choice
//multiple choices question
$I->haveRecord('answers', [   
'id'=> '25',
'question_id'=>'23',
'answer' => 'very hard',

]);

//tests//////////////////////////////////// 

//create a question
//When
$I->amOnPage('/home/questionnaires/10');
//and
$I->see('content');
//and
$I->seeElement('a', ['Add Your Question']);
//then
$I->click('a', ['Add Your Question']);
//and
$I->amOnPage('/home/questionnaires/10/questions/create');
//then
$I->see('questionnaire', '.title');
//and
  //add multiple choices answers
$I->submitForm('#addNewQuestionChoices', [      
'title'=> 'what do u think about it?' ,
'choice_1'=> 'very hard',
'choice_2'=>'difficulty',
'choice_3'=>'okay',
'choice_4'=>'easy',
]);

//and
$I->click('add question');

// check that the question had been written to the db 
$questionnaire = $I->grabRecord('questionnaires', ['title'=> 'The impact of covid-19 situation changing the world']);
$I->seeRecord('questionnaire_question', ['questionnaire_id'=> $questionnaire['id'], 'question_id'=>'23' ]);

//then
$I->seeCurrentUrlEquals('/questionnaires/questionnaire/10');
$I->see('questionnaire', '.title');
$I->see( 'The impact of covid-19 situation changing the world');
$I->see('what do u think about it?');
$I->see('very hard', 'difficulty', 'okay', 'easy');
