<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('check responses');

//add a test user
$I->haveRecord('users', [
    'id'=> '15',
    'name'=> 'wellington',
    'email'=> 'wellington@example.com',
    'password'=> 'omelhor1980'
    ]);
    
    //Add test questionnaire
$I->haveRecord('questionnaires', [
    'id'=> '10',
    'user_id'=> '01',
    'Title' => 'The impact of covid-19 situation changing the world',
    'aim' => 'gathering infromation for analysis of the world health situation',
    ]);
    

//add test question
$I->haveRecord('questions', [
'id'=> '23',
'questionnaire_id'=>'10',
'question' => 'Did covid situation disturb ur life?',
]);

//add test answer choice
//multiple choices question
$I->haveRecord('answers', [   
'id'=> '25',
'question_id'=>'23',
'answer' => 'very hard',

]);

$I->haveRecord('survey_responses', [
    'id'=> '12',
    'survey_id'=> '06',
    'question_id'=>'23',
    'answer_id'=>'25',
    ]);
    


//check that the response has been added to the db 
$I->seeRecord('survey_responses', [ 'id'=>'1', 'survey_id'=> '1', 'question_id' => '1', 'answer_id'=>'1']);

//check the answers of the questionnaire on the interface
$I->amOnPage('/home/questionnaire/10');
$I->see('content');
$I->see( 'questionnaire{title}');
$I->see('question', '.title');
$I->see('Did covid situation disturb ur life?');
$I->see('answers', 'choice');
$I->see('very hard', 'difficulty', 'okay', 'easy');
$I->see('response', 'number');
$I->see('20%', '40%', '30%', '10%');
