<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new questionnaire');

//add a test user
$I->haveRecord('users', [
'id'=> '15',
'name'=> 'wellington',
'email'=> 'wellington@example.com',
'password'=> 'omelhor1980'
]);

//Add test questionnaire
$I->haveRecord('questionnaires', [
'id'=> '10',
'user_id'=> '01',
'Title' => 'The impact of covid-19 situation changing the world',
'aim' => 'gathering infromation for analysis of the world health situation',
]);

//tests//////////////////////////////////// 

//create a questionnaire 
//When
$I->amOnPage('/admin/home');
//then
$I->click('Create New Questionnaire');
//then
$I->amOnPage('/home/questionnaires/create');
//and
$I->see('create a new questionnaire');
//then
$I->submitForm('#createNewQuestionnare', [
'title'=> 'The impact of covid-19 situation changing the world',
'aim'=> 'gathering infromation for analysis of the world health situation',
]);
//and
$I->click('create');

// check that the questionnaire has been written to the db 
$questionnaire = $I->grabRecord('questionnaires', ['title'=> 'covid 19 world impact']);
$I->seeRecord('questionnaire', ['questionnaire_id'=> $questionnaire['id']]);

//then
$I->seeCurrentUrlEquals('/questionnaires/questionnaire{id}');
$I->see('questionnaire', '.title');
$I->see( 'The impact of covid-19 situation changing the world');
