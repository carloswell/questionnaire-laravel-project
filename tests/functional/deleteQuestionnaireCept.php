<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a questionnaire');

//add a test user
$I->haveRecord('users', [
 'id'=> '15',
'name'=> 'wellington',
'email'=> 'wellington@example.com',
'password'=> 'omelhor1980'
 ]);
    
 //Add test questionnaire
 $I->haveRecord('questionnaires', [
'id'=> '10',
'user_id'=> '01',
'Title' => 'The impact of covid-19 situation changing the world',
'aim' => 'gathering infromation for analysis of the world health situation',
]);

//When
$I->amOnPage('/home/questionnaires/10');
//and
$I->see('content');

//then
$I->seeElement('button', ['questionnaire{id}', 'Delete']);
//and
$I->click('a', ['The impact of covid-19 situation changing the world']);

//then
$I->amOnPage('/home/questionnaires');
//and
$I->dontSeeElement('a',['name'=> '10']);
