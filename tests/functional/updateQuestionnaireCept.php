<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a questionnaire');

//add a test user
$I->haveRecord('users', [
    'id'=> '15',
    'name'=> 'wellington',
    'email'=> 'wellington@example.com',
    'password'=> 'omelhor1980'
    ]);
    
    //Add test questionnaire
    $I->haveRecord('questionnaires', [
    'id'=> '10',
    'user_id'=> '01',
    'Title' => 'The impact of covid-19 situation changing the world',
    'aim' => 'gathering infromation for analysis of the world health situation',
    ]);
    

//tests//////////////////////////////////// 

//create a questionnaire 
//When
$I->amOnPage('/home/questionnaires/questionnaire{id}');
//and
$I->see('questionnaire->title', 'h1');
//and
$I->see('The impact of covid-19 situation changing the world');

//then
$I->seeElement('a', ['name'=> '10']);
//and
$I->click('a', ['name'=> '10', 'edit']);

//then
$I->amOnPage('/home/questionnaires/10/edit');
//and
$I->see('questionnaire - The impact of covid-19 situation changing the world', 'h1');
//then
$I->fillField('title', 'updatedtitle');
//and
$I->fillField('aim', 'updatedaim');
//then
$I->click('update');

//then
$I->seeCurrentUrlEquals('home/questionnaire');
$I->seeRecord('questionnaire', ['title'=> 'upddatetitle', 'aim'=>'updateaim']);
$I->see('questionnaire', 'h1');
$I->see('updatetitle');
