<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('take a questionnaire preview');

//add a test user
$I->haveRecord('users', [
    'id'=> '15',
    'name'=> 'wellington',
    'email'=> 'wellington@example.com',
    'password'=> 'omelhor1980'
    ]);
    
    //Add test questionnaire
$I->haveRecord('questionnaires', [
    'id'=> '10',
    'user_id'=> '01',
    'Title' => 'The impact of covid-19 situation changing the world',
    'aim' => 'gathering infromation for analysis of the world health situation',
    ]);
    

//add test question
$I->haveRecord('questions', [
'id'=> '23',
'questionnaire_id'=>'10',
'question' => 'Did covid situation disturb ur life?',
]);

//add test answer choice
//multiple choices question
$I->haveRecord('answers', [   
'id'=> '25',
'question_id'=>'23',
'answer' => 'very hard',

]);

$I->haveRecord('survey_responses', [
    'id'=> '12',
    'survey_id'=> '06',
    'question_id'=>'23',
    'answer_id'=>'25',
    ]);
    

//tests//////////////////////////////////// 

//preview the questionnaire
//When
$I->amOnPage('/home/questionnaires/10');
//then
$I->see('content');
$I->see('questionnaire');
$I->see('questions');
$I->see('answers');
//and
$I->click('Questionnaire Preview');

//then
$I->seeCurrentUrlEquals('/surveys/8-the-impact-of-covid-19-situation-changing-the-world');
$I->see('content');

//and
$I->fillField('title', 'very hard');
$I->fillField('name', 'moreira');
$I->fillField('email', '24359939@example.ac.uk');

//then
$I->click('Submit Survey');
//and
$I->seeCurrentUrlEquals('/surveys/8-the-impact-of-covid-19-situation-changing-the-world');
$I->seeElement('h1', 'thank you for your time answer this survey');

// check that the test preview has been written to the db 
$I->seeRecord('survey_responses', [ 'id'=>'1', 'survey_id'=> '1', 'question_id' => '1', 'answer_id'=>'1']);
