<?php

class loginCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->amOnPage('/welcome/login');
        $I->see('Login', 'h1');
        $I->submitForm('.login', ['e-mail-address'=> 'wellingtonexample.com',
         'password'=> '12345'
        ]);
        //and
        $I->click('login');

        //Then
        $I->seeCurrentUrlEquals('/admin/home');
        $I->see('dashboard', 'h1');
        $I->see('you are logging');

        //Check for duplication

        //When
        $I->amOnPage('/welcome/login');
        $I->see('login', 'title');
        $I->submitForm('.login', [
        'e-mail-address'=> 'wellingtonexample.com',
        'password'=> '12345'
        ]);
        //and
        $I->click('login');
        //Then
        $I->seeCurrentUrlEquals('/admin/home');
        $I->see('dashboard', 'h1');
        $I->see('Error email already been used!');

    }
}
