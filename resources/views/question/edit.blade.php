@extends('layouts.app') <!--extend /layout/app.bade.php -->

@section('content')<!--start of the section-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$question->question}}</div>

                <div class="card-body">

                 <!--form to create question and answers choices  -->
                   <form action="/questions/{{ $question->id }}" method="POST"  enctype="multipart/form-data"> 

                   @csrf
                    @method('PUT')
                 <!--question input start here  -->

                        <div class="form-group">
                            <label for="question">Question</label>
                            <input name="question[question]" type="text" class="form-control" 
                            value="{{$question->question}}"
                            id="question" aria-describedby="questionHelp">
                             <small id="questionHelp" class="form-text text-muted">Create a question to be simple to understand and free of bias.</small>
                             
                             @error('question.question')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <fieldset>
                                <legend>Choices</legend>
                                <small id="choicesHelp" class="form-text text-muted">Add the choices that will best describe your question.</small>

                                    <!--Multiple choice answers start here with four choices  -->

                                    <div>
                                        <div class="form-group">
                                            <label for="answer1">Choice 1</label>
                                            <input name="answers[][answer]" type="text" 
                                               value="{{$question->answers[0]->answer}}"
                                                class="form-control" id="answer1" aria-describedby="choiccesHelp">
                                            
                                              <!-- error validation require when fill the inputs -->

                                            @error('answers.0.answer')
                                            <small class="text-danger">{{$message }}</small>
                                            @enderror
                                        </div>
                                    </div>

                                       <div>
                                        <div class="form-group">
                                            <label for="answer2">Choice 2</label>
                                            <input name="answers[][answer]" type="text" 
                                                value="{{$question->answers[1]->answer}}"
                                                class="form-control" id="answer2" aria-describedby="choiccesHelp">

                                            @error('answers.1.answer')
                                            <small class="text-danger">{{$message }}</small>
                                            @enderror
                                        </div>
                                    </div>

                                          <div>
                                        <div class="form-group">
                                            <label for="answer3">Choice 3</label>
                                            <input name="answers[][answer]" type="text" 
                                                value="{{$question->answers[2]->answer}}"
                                                class="form-control" id="answer3" aria-describedby="choiccesHelp" >

                                            @error('answers.2.answer')
                                            <small class="text-danger">{{$message }}</small>
                                            @enderror
                                        </div>
                                    </div>

                                          <div>
                                        <div class="form-group">
                                            <label for="answer4">Choice 4</label>
                                            <input name="answers[][answer]" type="text" 
                                                value="{{$question->answers[3]->answer}}"
                                                class="form-control" id="answer4" aria-describedby="choiccesHelp">

                                            @error('answers.3.answer')
                                            <small class="text-danger">{{$message }}</small>
                                            @enderror
                                        </div>
                                    </div>      
                            </fieldset>
                        </div>
                        
                        <!--button to submit the question and answers created to the questionnaire  -->
                        <button type="update" class="btn btn-primary">Update</button>
                   </form> <!-- end of the form-->
                  
                   <div class="card-footer mt-4">@include('layouts.footer')</div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection<!-- End of section -->
