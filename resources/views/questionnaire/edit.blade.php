@extends('layouts.app')<!--extend /layout/app.bade.php -->

@section('content')<!--start of the section-->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header title">{{$questionnaire->title}}</div>
                 
                 <!--form to edit the questionnaire in the questionnaire system  -->
                <form method="POST" action="/questionnaires/{{$questionnaire->id}}" enctype="multipart/form-data" >
                    @csrf
                    @method('PUT')
                 
                   <!--questionnaire edit the  input fields start here  -->
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input name="title" type="text" value="{{ $questionnaire->title }}" class="form-control" id="title"/>
                             <small id="titleHelp" class="form-text text-muted">The title should correspond to the meaning of the questionnaire.</small>
                              
                               <!-- error validation require when fill the inputs -->
                             @error('title')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="aim">Aim</label>
                            <input name="aim" type="text" value="{{ $questionnaire->aim }}" class="form-control" id="aim" />
                             <small id="aimHelp" class="form-text text-muted">A good aim explanation will improve the questionnaire answers.</small>

                             @error('aim')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>
                </form>  <!-- end of the form-->
                
                <div class="card-footer mt-4">@include('layouts.footer')</div>

            </div>
        </div>
    </div>
</div>
@endsection<!-- End of section -->