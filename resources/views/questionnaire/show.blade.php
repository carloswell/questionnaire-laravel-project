@extends('layouts.app')<!--extend /layout/app.bade.php -->

@section('content')<!--start of the section-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <h1 class="card-header">{{$questionnaire->title}}</h1>
                    
                    <!--div to store the href links on the view add your question and questionniare preview-->
                <div class="card-body">
                <a class="btn btn-primary" href="/questionnaires/{{$questionnaire->id }}/questions/create">Add Your Question</a>
                <a class="btn btn-primary" href="/surveys/{{$questionnaire->id }}-{{ Str::slug($questionnaire->title) }}">Questionnaire Preview</a>

                </div >
                <!--form for the delete questionniare button -->
                <form action="{{ $questionnaire->id }}" method="post">
                            @method('DELETE')
                            @csrf

                            <button type="submit" class="btn btn-sn btn-outline-danger ml-2">Delete</button>
                            <a class="btn btn-sn btn-warning ml-2" href="/questionnaires/{{$questionnaire->id }}/edit" >Edit</a>

                </form><!-- end of the form-->
            
            </div>

            <!-- foreach loop to loop over and get the questions of the questionniare selected  -->
            @foreach($questionnaire->questions as $question)
            
                <div class="card mt-4">
                    <div class="card-header">{{$question->question }}</div>

                    <div class="card-body">            
                        <ul class="list-group">
                             @foreach($question->answers as $answer)<!-- nested foreach loop for the answers of the questionniare -->
                                <li class="list-group-item d-flex justify-content-between">
                                    <div>{{$answer->answer }}</div>
                                    @if($question->responses->count())
                                    
                                        <!-- use of intval method to count and calculate the responses in precentage-->
                                        <div>{{ intval(( $answer->responses->count() * 100) / $question->responses->count()) }}%</div>
                                    @endif
                                </li>
                            @endforeach<!--End of nested foreach loop-->
                        </ul>
                     </div>
                     <div class="card-footer">
                    
                    <!--form for the delete questions in the questinniare -->
                        <form action="{{ $questionnaire->id }}/questions/{{ $question->id }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-sn btn-outline-danger">Delete</button>
                            
                            <a class="btn btn-sn btn-warning ml-2" href="/questions/{{ $question->id }}/edit" >Edit</a>

                        </form> <!-- end of the form-->
                     </div>
                </div>
            @endforeach<!--End foreach loop-->
           
            <div class="card-footer mt-4">@include('layouts.footer')</div>

        </div>
    </div>
</div>
@endsection<!-- End of section -->
