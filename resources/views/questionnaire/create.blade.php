@extends('layouts.app')<!--extend /layout/app.bade.php -->

@section('content')<!--start of the section-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Questionnaire</div>

                <div class="card-body">

                  <!--form to create questionnaire in the questionnaire system  -->
                   <form action="/questionnaires" method="post">

                        @csrf   <!--mandatory to be used in any laravel form to protect it against cross-site request forgeries -->

                        <!--questionnaire input fields start here  -->
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input name="title" type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter Title">
                             <small id="titleHelp" class="form-text text-muted">The title should correspond to the meaning of the questionnaire.</small>
                             
                              <!-- error validation require when fill the inputs -->
                             @error('title')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="aim">Aim</label>
                            <input name="aim" type="text" class="form-control" id="aim" aria-describedby="aimHelp" placeholder="Enter Aim">
                             <small id="aimHelp" class="form-text text-muted">A good aim explanation will improve the questionnaire answers.</small>

                             @error('aim')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                        </div>
 
                        <!--button to submit the questionnaire created in the system  -->
                        <button type="submit" class="btn btn-primary">Create</button>
                   </form> <!-- end of the form-->

                   <div class="card-footer mt-4">@include('layouts.footer')</div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection<!-- End of section -->
