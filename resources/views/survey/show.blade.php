@extends('layouts.app')<!--extend /layout/app.bade.php -->

@section('content')<!--start of the section-->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            <h1>{{$questionnaire->title }}</h1>
          
          <!--questionnaire survey ethical text for the use responder view take survey -->  
            <div>
            <small class="form-text text-muted">"The information you provide in this survey questionnaire, is only for the
             purpose of researching about the subject related and any of the information you provide,
             will be keep safe and not shared  or used in any malicious way".</small>
            </div>

             <!--form to display the questionniare question and answer choices for the user responder -->
            <form action="/surveys/{{$questionnaire->id}}-{{Str::slug($questionnaire->title) }}" method="post">
                @csrf 
           
            <!-- foreach loop to wrap over and set the questions of the questionniare survey  -->

                @foreach($questionnaire->questions as $key => $question)
                    <div class="card mt-4">
                        <div class="card-header"><strong>{{$key + 1}}</strong>{{$question->question }}</div>

                        <div class="card-body"> 

                            @error('responses.' . $key . '.answer_id')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror 

                            <ul class="list-group">
                            <!-- nested foreach loop to wrap over and set the answers of the question survey  -->

                                @foreach($question->answers as $answer)
                                <label for="answer{{ $answer->id }}">
                                    <li class="list-group-item">
                                    
                                    <!-- check box answer choices -->
                                        <input type="radio" name="responses[{{ $key }}][answer_id]" id="answer{{$answer->id}}"
                                            {{ (old('responses.' . $key . '.answer_id' ) == $answer->id) ? 'checked' : '' }}
                                            class="mr-2" value="{{ $answer->id }}">
                                        {{ $answer->answer }}
                                        
                                        <!-- hidden input of the response of the question id -->
                                        <input type="hidden" name="responses[{{ $key }}][question_id]" value="{{$question->id }}">
                                    </li>
                                </label>
                                 @endforeach<!-- end of nested foeach loop  -->

                            </ul>
                        </div>
                    </div>
                @endforeach<!-- end of foeach loop  -->

            <div class="card mt-4"> 
                       
                <!-- table field user details of name and email for survey table -->

                <div class="card-header">Your information</div>
                 
                 <!--survey input detail fields start here  -->
                <div class="card-body">
                        <div class="form-group">
                            <label for="name">Your Name</label>
                            <input name="survey[name]" type="text" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Enter Name">
                             <small id="nameHelp" class="form-text text-muted">Please add your name!</small>
                             
                              <!-- error validation require when fill the inputs -->
                             @error('name')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input name="survey[email]" type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Email">
                             <small id="emailHelp" class="form-text text-muted">Please add your Email.</small>

                             @error('email')
                                <small class="text-danger">{{$message }}</small>
                            @enderror
                       </div>
              
                </div>

                    <div>
                        <button class="btn btn-dark m-2"type="submit">Submit Survey</button>
                    </div>
                </div>
                
                <div class="card-footer mt-4">@include('layouts.footer')</div>

            </div>
            </form> <!-- end of the form-->
        </div>

    </div>
</div>
@endsection<!-- End of section -->